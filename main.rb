require 'curb'
require 'nokogiri'
require 'csv'


def get_pages(url)
  page_number = 1
  pages = []

  while Curl.head((completed_url = complete_category_url(url, page_number))).status.to_s.include? '200'
    puts "Downloading page #" + page_number.to_s
    pages.push(Curl.get(completed_url).body.to_s)
    page_number += 1
  end

  pages
end


def complete_category_url(url, page_number)
  if url[-1] != '/'
    url += '/'
  end

  url + '?p=' + page_number.to_s
end


def get_product_links(page)
  puts "Getting product links"
  doc = Nokogiri::HTML(page)
  doc.xpath("//*[@class = 'product_img_link product-list-category-img']/@href")
end


def get_product_data(link)
  puts "Getting product data"
  page = Curl.get(link).body.to_s
  doc = Nokogiri::HTML(page)
  data = []

  get_prices(doc).each {|type|
    data.push([get_title(doc) + ' - ' + type[0], type[1].to_s.chomp('/u'), get_image(doc)])
  }

  data
end


def get_image(doc)
  doc.xpath("//*[@id = 'bigpic']/@src").text
end


def get_title(doc)
  doc.xpath("normalize-space(//h1[@class = 'product_main_name'])").split.map(&:capitalize)*' '
end


def get_prices(doc)
  variants = doc.xpath("//*[@class = 'label_comb_price']")
  pairs = []

  variants.each {|variant|
    pairs.push(variant.xpath(".//descendant::*/text()"))
  }

  pairs
end


def write_to_csv(data)
  puts "Writing data to CSV"
  File.write(ARGV[1], data.map(&:to_csv).join, :mode => 'w')
end


start_time = Time::now
data = []
pages = get_pages(ARGV[0])
pages.each {|page|
  product_links = get_product_links(page)

  product_links.each {|product|
    product_data = get_product_data(product)
    data += product_data
  }
}
write_to_csv(data)
puts "DONE IN " + (Time::now - start_time).to_s
